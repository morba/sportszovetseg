﻿using System.Collections.Generic;
using System.Linq;
using IdentityServer4;
using IdentityServer4.Models;

namespace Sportszovetseg.IDP
{
    public class Config
    {
        public static IEnumerable<ApiResource> GetApiResources() => new List<ApiResource>
        {
            new ApiResource("api1", "Sportszövetség tagnyilvántartó API")
        };
        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients() =>
            // client credentials client
            new List<Client>
            {
                // resource owner password grant client
                new Client
                {
                    ClientId = "ro.frontend",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials
                        .Union(GrantTypes.ImplicitAndClientCredentials).ToList(),

                    ClientSecrets =
                    {
                        new Secret("2ldJDpAMnKUHakbjtx09f6zExPvM0Ps/".Sha256())
                    },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "roles",
                        "api1",
                    }
                },
            };
        public static IEnumerable<IdentityResource> GetIdentityResources() => new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResource("roles", "Your role(s)", new List<string>() { "role"}),
            };

    }
}
