﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sportszovetseg.DataAccess;

namespace Sportszovetseg.IDP
{

    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            var sportDbConnectionString =
               Configuration.GetConnectionString("main");
            var identityServerDataDBConnectionString =
               Configuration.GetConnectionString("mainIDP");
            var migrationsAssembly = typeof(Startup)
                .GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<SportDbContext>(x => x.UseNpgsql(identityServerDataDBConnectionString, options => options.MigrationsAssembly(migrationsAssembly)));
            services.AddDbContext<ConfigurationDbContext>(x => x.UseNpgsql(identityServerDataDBConnectionString, options => options.MigrationsAssembly(migrationsAssembly)));
            services.AddDbContext<PersistedGrantDbContext>(x => x.UseNpgsql(identityServerDataDBConnectionString, options => options.MigrationsAssembly(migrationsAssembly)));

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddInMemoryApiResources(Config.GetApiResources())
                //.AddDefaultSecretParsers() // do we need these ?
                //.AddDefaultSecretValidators()
                //.AddInMemoryClients(Config.GetClients())
                //.AddInMemoryPersistedGrants()
                .AddConfigurationStore(builder =>
                    builder.ConfigureDbContext = dbuilder =>
                    dbuilder.UseSqlServer(identityServerDataDBConnectionString, options =>
                    options.MigrationsAssembly(migrationsAssembly)))
                .AddOperationalStore(builder =>
                    builder.ConfigureDbContext = dbBuilder =>
                    dbBuilder.UseSqlServer(identityServerDataDBConnectionString, options =>
                    options.MigrationsAssembly(migrationsAssembly)))
                .AddProfileService<SportProfileServices>()
                .AddResourceOwnerValidator<PasswordValidator>()
                ;

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app, 
            IHostingEnvironment env, 
            SportDbContext sportDbContext,
            ConfigurationDbContext configurationDbContext,
            PersistedGrantDbContext persistedGrantDbContext)
        {
            configurationDbContext.Database.Migrate();
            persistedGrantDbContext.Database.Migrate();
            sportDbContext.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseIdentityServer();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
