﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer4.Validation;
using Microsoft.EntityFrameworkCore;
using Sportszovetseg.BusinessLogic;
using Sportszovetseg.DataAccess;
using Sportszovetseg.Models.Database;
using Sportszovetseg.Common;
using IdentityServer4.Services;
using IdentityServer4.Models;

namespace Sportszovetseg.IDP
{
    public class SportProfileServices : IProfileService
    {
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {


        }
        
        public async Task IsActiveAsync(IsActiveContext context)
        {

        }
    }


    public class PasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly SportDbContext _dbContext;

        /// <summary>
        /// std ctor with DI
        /// </summary>
        /// <param name="dbContext"></param>
        public PasswordValidator(SportDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            User user = await _dbContext.users.Include(u => u.passwordSet).FirstOrDefaultAsync(u => u.userName == context.UserName);
            PasswordManager.LoginResult result = PasswordManager.login(ref user, context.Password);
            if (result != PasswordManager.LoginResult.Success)
            {
                context.Result.IsError = true;
                context.Result.Error = result.ToString();
            }
            else
            {
                context.Result.IsError = false;
                List<Claim> claims = new List<Claim>() {
                        new Claim("name", user.forename + " " + user.surname),
                        new Claim("sub", user.id.ToString()),
                        new Claim("scope", "api1"),
                        new Claim("aud", "api1"),
                        new Claim("amr", "pwd"), //authentication method = password
                        new Claim("idp", "local"), //identity provider
                        new Claim("auth_time", ((long)DateTime.UtcNow.ToEpochSeconds()).ToString()),
                    };

                context.Result.Subject = new ClaimsPrincipal(new List<ClaimsIdentity>()
                {
                    new ClaimsIdentity(claims)
                });
            }

        }
    }
}
