﻿/*
TODO create a better version using one of these scripts:
npm: https://www.npmjs.com/package/react-bootstrap-date-picker (github: https://github.com/pushtell/react-bootstrap-date-picker)
nuget: http://eonasdan.github.io/bootstrap-datetimepicker/ (github: https://github.com/Eonasdan/bootstrap-datetimepicker)
probably need to manually write type definitions myself...
*/

import * as React from 'react';


interface IDateInputProps {
    name: string,
    label: string,
    onChange: React.EventHandler<React.ChangeEvent<HTMLInputElement>>,
    placeholder?: string,
    value?: Date,
    error?: string,
    /// TODO extend it as we go..
    required?: boolean,
    disabled?: boolean,
    labelWidthClass?: string;
    inputWidthClass?: string;
}

///there is some problem with interface type matching in react props, bcs it requires an exact match to the interface.
const DateInput = (props: IDateInputProps) => {
    let { name, label, onChange, placeholder, value, error, required, disabled, labelWidthClass, inputWidthClass } = props;
    let wrapperClass = 'form-group';
    if (error && error.length > 0) {
        wrapperClass += " " + 'has-error';
    }

    return (
        <div className={wrapperClass}>
            <label htmlFor={name} className={labelWidthClass} >{label}</label>
            <div className={`field ${inputWidthClass}`} >
                <input
                    type="date"
                    name={name}
                    className="form-control"
                    placeholder={placeholder}
                    value={value && value.toISOString().substr(0,10)}
                    onChange={onChange}
                    required={required}
                    disabled={disabled}
                />
                {error && <div className="alert alert-danger">{error}</div>}
            </div>
        </div>
    );
};

export default DateInput;