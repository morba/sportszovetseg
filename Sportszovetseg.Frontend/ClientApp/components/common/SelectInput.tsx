﻿import * as React from 'react';

interface SelectOption {
    value: string,
    text:string,
}

interface SelectInputProps {
    name:string,
    label: string,
    onChange: React.EventHandler<React.ChangeEvent<HTMLSelectElement>>,
    defaultOption?: string,
    value?: string,
    error?: string,
    options?: SelectOption[],
    required?: boolean,
    disabled?: boolean,
    labelWidthClass?: string;
    inputWidthClass?: string;
}

const SelectInput = (props: SelectInputProps) => {
    let { name, label, onChange, defaultOption, value, error, options, required, disabled, labelWidthClass, inputWidthClass } = props;
    let wrapperClass = 'form-group';
    if (error && error.length > 0) {
        wrapperClass += ' has-error';
    }
    return (
        <div className={wrapperClass}>
            <label
                htmlFor={name}
                className={labelWidthClass}
            >
                {label}
            </label>
            <div className={`field ${inputWidthClass}`}>
                {/* Note, value is set here rather than on the option - docs: https://facebook.github.io/react/docs/forms.html */}
                <select
                    name={name}
                    value={value}
                    onChange={onChange}
                    className="form-control"
                    required={required}
                    disabled={disabled}
                >
                    <option value="">{defaultOption}</option>
                    {options && options.map((option) => {
                        return <option key={option.value} value={option.value}>{option.text}</option>;
                    })
                    }
                </select>
                {error && <div className="alert alert-danger">{error}</div>}
            </div>
        </div>
    );
};

export default SelectInput;