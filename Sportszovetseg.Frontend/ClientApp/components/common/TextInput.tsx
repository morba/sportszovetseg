﻿import * as React from 'react';


interface ITextInputProps {
    name: string,
    label: string,
    onChange: React.EventHandler<React.ChangeEvent<HTMLInputElement>>,
    placeholder?: string,
    value?: string,
    error?: string,
    /// TODO extend it as we go...
    type?: "text" | "password" | "date", //create date input to be type safe ?
    required?: boolean,
    disabled?: boolean,
    labelWidthClass?: string;
    inputWidthClass?: string;
}

///there is some problem with interface type matching in react props, bcs it requires an exact match to the interface.
const TextInput = (props: ITextInputProps) => {
    let { name, label, onChange, placeholder, value, error, type, required, disabled, labelWidthClass, inputWidthClass } = props;
    type = type || "text";
    let wrapperClass = 'form-group';
    if (error && error.length > 0) {
        wrapperClass += " " + 'has-error';
    }

    return (
        <div className={wrapperClass}>
            <label htmlFor={name} className={labelWidthClass} >{label}</label>
            <div className={`field ${inputWidthClass}`} >
                <input
                    type={type}
                    name={name}
                    className="form-control"
                    placeholder={placeholder}
                    value={value}
                    onChange={onChange}
                    required={required}
                    disabled={disabled}
                />
                {error && <div className="alert alert-danger">{error}</div>}
            </div>
        </div>
    );
};

export default TextInput;