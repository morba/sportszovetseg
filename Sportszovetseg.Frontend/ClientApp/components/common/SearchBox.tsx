﻿import * as React from 'react';
import TextInput from './TextInput';

interface klp {
    key: string,
    label: string,
}

interface SearchBoxProps {
    label: string;
    placeholder?: string;
    name: string
    results: klp[];
    onChange: (str: string) => void;
    select: (key: string) => void;
    value: string
}

const SearchBox = (props: SearchBoxProps) => {
    return <div>
        <form className="form-horizontal " onSubmit={(event) => event.preventDefault()}>
            <TextInput
                name={props.name}
                label={props.label}
                onChange={(ev) => { props.onChange(ev.target.value) }}
                required
                value={props.value}
                labelWidthClass="col-xs-2"
                inputWidthClass="col-xs-10"
            />
            {props.results.map(emp => <div key={emp.key} className="form-group row">
                <div className="col-xs-2" />
                <div className="col-xs-10">
                    <button className="btn" onClick={() => { props.select(emp.key) }} >
                        {emp.label}
                    </button>
                </div>
            </div>)}
        </form>
    </div>;
}

export default SearchBox;