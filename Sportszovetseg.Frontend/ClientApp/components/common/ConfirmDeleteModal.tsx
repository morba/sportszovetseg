﻿import * as React from 'react';

export default class ConfirmDeleteModal extends React.Component<{ id: string, onDelete: () => void, message?: string }, {}> {
    render() {
        return <div>
            <div id={`modal-delete-${this.props.id.toString()}`} className="modal fade" role="dialog">
                <div className="modal-dialog modal-sm">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                            <h4 className="modal-title">Törlés</h4>
                        </div>
                        <div className="modal-body">
                            <p>{this.props.message || 'Biztos, hogy törölni akarod?'}</p>
                            <button className="btn btn-danger" data-dismiss="modal" onClick={this.props.onDelete}>Igen</button>
                            <span> </span>
                            <button className="btn btn-secondary" data-dismiss="modal">Nem</button>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal">Mégse</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}