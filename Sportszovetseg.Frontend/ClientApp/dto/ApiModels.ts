﻿import Guid from './Guid';

export interface Permission {
    id: Guid,
    name: string,
    start: Date,
    end: Date,
}

export const Pemission_Empty: Permission = {
    id: Guid.Empty,
    name: '',
    start: new Date('1900-01-01'),
    end: new Date('1900-01-01'),
}