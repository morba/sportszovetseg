﻿import Guid from "./Guid";

//===============NEW======================

export interface UserDbo {
    id: Guid;
    userName: string;
    email: string;
    phoneNumber: string;
    birthdate: Date;
    surname: string;
    forename: string;
    gender: boolean; //to trigger
}

export interface AthleteDbo {
    id: Guid;
    userId: Guid;
    user: UserDbo;
}



//=============OLD=======================

export enum EStatus {
    Null = 0,
    Normal = 1,
    Deleted = 2,
    Processed = 3,
}

export interface EmployeeDbo {
    id: Guid;
    name: string;
    maconomyId: string;
    //we dont care
    //version: number;
    state: EStatus;
    loginPasswordSet?: LoginPasswordDbo[]
    permissionMapSet?: EmployeePermissionMapDbo[];
}

export interface LoginPasswordDbo {
    id: Guid;
    employeeId: Guid;
    lastModified: Date;
    modifyRate: Date;
    ////we dont really need this.
    //passwordHash: string;
    //passwordSalt: string;
    //iterationCount: number;
    lastAttempt: Date;
    attemptCounter: number;
    version: number;
    state: EStatus;
    employeeLink?: EmployeeDbo;
}

export interface PermissionDbo {
    name: string;
    description: string;
    state: EStatus;
    employeeMapSet?: EmployeePermissionMapDbo[];
}

export interface EmployeePermissionMapDbo {
    id: Guid;
    employeeId: Guid;
    permissionId: string;
    validity_start: Date;
    validity_end: Date;
    state: EStatus;
    employeeLink?: EmployeeDbo;
    permissionLink?: PermissionDbo;
}