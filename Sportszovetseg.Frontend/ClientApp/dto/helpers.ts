﻿
const apiBaseUrl:string = "http://localhost:53504"

interface IBootstrapColumn {
    cols: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
    size: 'xs' | 'sm' | 'md' | 'lg' | 'xl'
}

class BootstrapColumn{
    public cols: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
    public size: 'xs' | 'sm' | 'md' | 'lg' | 'xl'
    public readonly toString = () => `col-${this.size}-${this.cols}`
}