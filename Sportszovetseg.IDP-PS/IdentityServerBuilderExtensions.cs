﻿using Sportszovetseg.IDP.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sportszovetseg.IDP
{
    public static class IdentityServerBuilderExtensions
    {
        public static IIdentityServerBuilder AddSportUserStore(this IIdentityServerBuilder builder)
        {
            //builder.Services.AddSingleton<IMarvinUserRepository, MarvinUserRepository>();
            builder.AddProfileService<SportUserProfileService>();
            return builder;
        }
    }
}
