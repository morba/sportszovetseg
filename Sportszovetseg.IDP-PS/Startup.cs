﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Sportszovetseg.IDP.Entities;
using Sportszovetseg.IDP.Services;
using IdentityServer4;
using System.Security.Cryptography.X509Certificates;
using System.Reflection;
using IdentityServer4.EntityFramework.DbContexts;
using Sportszovetseg.DataAccess;
using Sportszovetseg.BusinessLogic;

namespace Sportszovetseg.IDP
{
    public class Startup
    {
        public static IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var sportDbConnectionString =
               Configuration.GetConnectionString("main");
            var identityServerDataDBConnectionString =
               Configuration.GetConnectionString("mainIDP");
            var migrationsAssembly = typeof(Startup)
                .GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<SportDbContext>(x => x.UseNpgsql(identityServerDataDBConnectionString, options => options.MigrationsAssembly(migrationsAssembly)));
            services.AddDbContext<ConfigurationDbContext>(x => x.UseNpgsql(identityServerDataDBConnectionString, options => options.MigrationsAssembly(migrationsAssembly)));
            services.AddDbContext<PersistedGrantDbContext>(x => x.UseNpgsql(identityServerDataDBConnectionString, options => options.MigrationsAssembly(migrationsAssembly)));

            services.AddMvc();

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddSportUserStore()
                .AddConfigurationStore(options=> options.ConfigureDbContext = 
                    builder => builder.UseNpgsql(identityServerDataDBConnectionString, o => o.MigrationsAssembly(migrationsAssembly)))
                .AddOperationalStore(options =>
                options.ConfigureDbContext =
                    builder => builder.UseSqlServer(identityServerDataDBConnectionString, o => o.MigrationsAssembly(migrationsAssembly)));

        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            ILoggerFactory loggerFactory, SportDbContext sportDbContext,
            ConfigurationDbContext configurationDbContext, 
            PersistedGrantDbContext persistedGrantDbContext)
        {
            loggerFactory.AddConsole();
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            configurationDbContext.Database.Migrate();
            configurationDbContext.EnsureSeedDataForContext();

            persistedGrantDbContext.Database.Migrate();

            sportDbContext.Database.Migrate();
            sportDbContext.EnsureSeedData();

            app.UseIdentityServer();

            app.UseStaticFiles();

            app.UseMvcWithDefaultRoute();
        }
    }
}
