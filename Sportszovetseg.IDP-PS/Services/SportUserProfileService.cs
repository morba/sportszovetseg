﻿using IdentityServer4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Extensions;
using System.Security.Claims;
using Sportszovetseg.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Sportszovetseg.IDP.Services
{
    public class SportUserProfileService : IProfileService
    {
        private readonly SportDbContext _db;

        public SportUserProfileService(SportDbContext db)
        {
            _db = db;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var subjectId = context.Subject.GetSubjectId();
            var user = await _db.users.Include(u => u.roleSet).FirstOrDefaultAsync(x => x.id.ToString() == subjectId);

            context.IssuedClaims = user.roleSet.Select
              (c => new Claim("role", c.roleId.ToString())).ToList();

        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var subjectId = context.Subject.GetSubjectId();
            if (! Guid.TryParse(subjectId, out Guid id) )
            {
                context.IsActive = false;
            }
            else
            {
                context.IsActive = !((await _db.users.FindAsync(id))?.isDeleted ?? true);
            }
           
        }
    }
}
