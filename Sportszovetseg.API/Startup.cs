﻿//system
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
//framework
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Extensions.Primitives;
//3rd party
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
//project
using Sportszovetseg.API.Controllers;
using Sportszovetseg.BusinessLogic;
using Sportszovetseg.DataAccess;
using Sportszovetseg.Npgsql;

namespace Sportszovetseg.API
{
    /// <summary>
    /// config at start
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// default ctor
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// appsettings.json
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore()
                .AddAuthorization()
                .AddVersionedApiExplorer(o =>
            {
                o.DefaultApiVersion = new ApiVersion(1, 0);
                o.GroupNameFormat = "'v'VVV";
            });
            services.AddLogging();
            services.AddOptions();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options => {
                    options.Authority = "https://localhost:44330";
                    options.RequireHttpsMetadata = true;
                    options.ApiName = "api1";
                    //options.ApiSecret = "secret";
                });

            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
                o.ApiVersionReader = new HeaderApiVersionReader("x-api-version");

                o.Conventions.Controller<AthleteController>().HasApiVersion(new ApiVersion(1, 0));
                o.Conventions.Controller<BaseActions>().HasApiVersion(new ApiVersion(1, 0));
                o.Conventions.Controller<ClubController>().HasApiVersion(new ApiVersion(1, 0));
                o.Conventions.Controller<DocumentController>().HasApiVersion(new ApiVersion(1, 0));
                o.Conventions.Controller<ExampleController>().HasApiVersion(new ApiVersion(1, 0));
                o.Conventions.Controller<ExampleV2Controller>().HasApiVersion(new ApiVersion(2, 0));
                o.Conventions.Controller<MedicalCertificateController>().HasApiVersion(new ApiVersion(1, 0));
                
            });

            services.AddSwaggerGen(options => {
                IApiVersionDescriptionProvider provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();

                foreach (var description in provider.ApiVersionDescriptions)
                {
                    options.SwaggerDoc(description.GroupName, 
                        new Info {
                            Title = $"Sportszövetség API {description.ApiVersion}",
                            Version = description.ApiVersion.ToString(),
                            Description = "Sportszovetseg api leiras"
                        });
                }
                options.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "Sportszovetseg.API.xml"));

            });

            IDbContextFactory<SportDbContext> dbContextFactory = new SportDbContextFactory(Configuration.GetConnectionString("main"));
            services.AddSingleton<IDbContextFactory<SportDbContext>>(dbContextFactory);
            services.AddDbContext<SportDbContext>(o => dbContextFactory.configureOptionsBuider(o));

            services.AddMvc();
        }

        private const string XForwardedPath = "X-Forwarded-Path";
        private const string XForwardedPathBase = "X-Forwarded-PathBase";
        private const string XForwardedProto = "X-Forwarded-Proto";

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="versionProvider"></param>
        /// <param name="sportDb"></param>
        public void Configure(
            IApplicationBuilder app, 
            IHostingEnvironment env, 
            ILoggerFactory loggerFactory,
            IApiVersionDescriptionProvider versionProvider,
            SportDbContext sportDb)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders =
                  Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor
                | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto
                | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedHost
            });


            //add data manually from proxy headers
            app.Use((context, next) =>
            {
                if (context.Request.Headers.TryGetValue(XForwardedPath, out StringValues path))
                {
                    context.Request.Path = new PathString(path);
                    //Console.WriteLine($"path={path}");
                }

                if (context.Request.Headers.TryGetValue(XForwardedPathBase, out StringValues pathBase))
                {
                    context.Request.PathBase = new PathString(pathBase);
                    //Console.WriteLine($"PathBase={pathBase}");
                }

                if (context.Request.Headers.TryGetValue(XForwardedProto, out StringValues proto))
                {
                    context.Request.Protocol = proto;
                    //Console.WriteLine($"proto={proto}");
                }
                return next();
            });


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                loggerFactory.AddDebug();
                loggerFactory.AddEventSourceLogger();
            }

            app.UseAuthentication();

            app.UseWelcomePage("/welcome");

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                foreach (var description in versionProvider.ApiVersionDescriptions)
                {
                    options.SwaggerEndpoint($"{description.GroupName}/swagger.json", description.GroupName);
                }
                options.EnableFilter();
            });

            sportDb.Database.Migrate();
            sportDb.EnsureSeedData();

            app.AddEfDiagrams<SportDbContext>();

            app.UseMvc();
        }
    }
}
