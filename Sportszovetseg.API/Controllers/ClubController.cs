﻿using Microsoft.AspNetCore.Mvc;
using Sportszovetseg.Models.Database;
using System;

namespace Sportszovetseg.API.Controllers
{
    [Route("api/club")]
    public class ClubController : ControllerBase
    {

        /// <summary>
        /// get the club of the currently logged in user
        /// </summary>
        /// <returns>OK</returns>
        [HttpGet]
        [ProducesResponseType(typeof(Club), 200)]
        public IActionResult me()
        {
            return Ok();
        }

        /// <summary>
        /// gets a club by id, or 404 if no club with the given id is visible to the currently logged in user
        /// </summary>
        /// <param name="id">club id</param>
        /// <returns>OK or Not Found</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Club), 200)]
        [ProducesResponseType(404)]
        public IActionResult get(Guid id)
        {
            return Ok();
        }

        /// <summary>
        /// creates a new club, returns the id of the newly created club
        /// </summary>
        /// <param name="clubName">name of the new club</param>
        /// <returns>OK(id of the new club) or Conflict(id of the club with the same name)</returns>
        [HttpPut]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(Guid), 409)]
        public IActionResult put(string clubName)
        {
            return Ok();
        }

        /// <summary>
        /// rename an existing club
        /// </summary>
        /// <param name="clubName">new name of the club</param>
        /// <returns>OK or Not Found or Conflict(id of the club with the same name)</returns>
        [HttpPost("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(Guid), 409)]
        public IActionResult post(Guid id, string clubName)
        {
            return Ok();
        }

        /// <summary>
        /// deletes a club, and set all its athletes to 'freelance'
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult delete(Guid id)
        {
            return Ok();
        }

    }
}
