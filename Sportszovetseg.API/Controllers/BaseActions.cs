﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sportszovetseg.DataAccess;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Sportszovetseg.API.Controllers
{
    /// <summary>
    /// actions everyone can take
    /// </summary>
    [Route("api")]
    public class BaseActions : ControllerBase
    {
        private SportDbContext _sportDb;

        /// <summary>
        /// standard ctor with DI
        /// </summary>
        /// <param name="db"></param>
        public BaseActions(SportDbContext db)
        {
            _sportDb = db;
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [Authorize]
        [HttpGet("me")]
        public async Task<IActionResult> me()
        {
            var userIdStr = User.FindFirst("sub")?.Value ?? "";
            var userId = Guid.ParseExact(userIdStr, "D");

            var user = await _sportDb.users.FindAsync(userId);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        /// <summary>
        /// change UI language for the current user
        /// </summary>
        /// <param name="language">the new language</param>
        /// <returns>OK</returns>
        [ProducesResponseType(200)]
        [HttpPost]
        [Route("changeLanguage/{language}")]
        public IActionResult changeLanguage(Language language)
        {
            return Ok();
        }

        /// <summary>
        /// changes password for the current user
        /// password minimum requirements are validated
        /// </summary>
        /// <param name="password">the new password</param>
        /// <returns>OK for success, BadReqest for wrong password</returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(string), 400)]
        [HttpPost]
        [Route("changePassword/{password}")]
        public IActionResult changePassword(string password)
        {
            if(password.Length < 6)
            {
                return BadRequest("password too short");
            }
            return Ok();
        }
    }
}
