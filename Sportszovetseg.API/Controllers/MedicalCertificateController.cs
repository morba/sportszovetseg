﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Sportszovetseg.Models.API;
using Sportszovetseg.Models.Database;

namespace Sportszovetseg.API.Controllers
{
    [Route("api/med")]
    public class MedicalCertificateController : ControllerBase
    {

        /// <summary>
        /// list all my medical certificates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<Guid>), 200)]
        public IActionResult get()
        {
            return Ok();
        }

        /// <summary>
        /// get a single medical certificate by id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(MedicalCertificate), 200)]
        [ProducesResponseType(404)]
        public IActionResult get(Guid id)
        {
            return Ok();
        }

        /// <summary>
        /// creates an new medical certificate for the currently logged in user
        /// </summary>
        /// <param name="createDto">creation dto</param>
        /// <returns>id of the new medical certificate</returns>
        [HttpPut]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult put([FromBody] MedicalCertificateCreateDto createDto)
        {
            return Ok();
        }

        /// <summary>
        /// accept a medical certificate as valid (verifies its identity)
        /// </summary>
        /// <param name="id">id of the medical certificate</param>
        /// <returns>OK, or Not Found</returns>
        [HttpPost("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult verify(Guid id)
        {
            return Ok();
        }

        /// <summary>
        /// deletes a medical certificate (eg. bcs it was not valid)
        /// </summary>
        /// <param name="id">id of the medical certificate</param>
        /// <returns>OK or Not Found</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult delete(Guid id)
        {
            return Ok();
        }

        /// <summary>
        /// list all the medical certificate of a given athlete,
        /// returns a list of ids if the athlete exists and the currently logged in user has read access rights
        /// otherwise return 404 Not Found
        /// </summary>
        /// <param name="athleteId">id of the athlete</param>
        /// <returns>OK, or Not Found</returns>
        [ProducesResponseType(typeof(List<Guid>), 200)]
        [ProducesResponseType(404)]
        [HttpGet("list/{athleteId}")]
        public IActionResult list(Guid athleteId)
        {
            return Ok();
        }

    }
}
