﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Sportszovetseg.Models.Database;

namespace Sportszovetseg.API.Controllers
{

    [Route("api/document")]
    public class DocumentController : ControllerBase
    {

        /// <summary>
        /// list all my document ids
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<Guid>), 200)]
        public IActionResult get()
        {
            return Ok();
        }

        /// <summary>
        /// get a document the currently logged in user have access to,
        /// gives 404 if document does not exists or the currently logged in user does not have read access
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Document), 200)]
        [ProducesResponseType(404)]
        public IActionResult get(Guid id)
        {
            return Ok();
        }
        
        /// <summary>
        /// uploads/creates a new document. 
        /// the document is passed as the body of the request,
        /// only the mime type is given as an URL parameter, every other meta data of the document will be added/calculated server side 
        /// (never trust the client)
        /// returns 409 Conflict if the document is already uploaded (document wth the same hash exists)
        /// </summary>
        /// <param name="mimeType">mime type of the document</param>
        /// <returns>newly created document id</returns>
        [HttpPut]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(Guid), 409)]
        public IActionResult put(string mimeType)
        {
            return Ok();
        }

        /// <summary>
        /// deletes an unused document from the DB, return 409 conflict if document deletion is not possible due to refereneces
        /// </summary>
        /// <param name="id">id of the document</param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(string), 409)]
        public IActionResult delete(Guid id)
        {
            return Ok();
        }

    }

    [Route("api/example")]
    public class ExampleV2Controller : ControllerBase
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1 but in version 2 format", "value2 same as verion 1", "value3 added by version 2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "vvalue";
        }
    }

    /// <summary>
    /// just an example
    /// </summary>
    [Route("api/example")]
    public class ExampleController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
