﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sportszovetseg.DataAccess;
using Sportszovetseg.Models.API;
using Sportszovetseg.Models.Database;

namespace Sportszovetseg.API.Controllers
{
    /// <summary>
    /// endpoint for managing athletes
    /// </summary>
    [Route("api/athlete")]
    public class AthleteController : ControllerBase
    {
        private SportDbContext _sportDb;
        /// <summary>
        /// std ctor w DI
        /// </summary>
        /// <param name="db"></param>
        public AthleteController(SportDbContext db)
        {
            _sportDb = db;
        }

        /// <summary>
        /// return the currently logged in athlete's basic info
        /// </summary>
        /// <returns></returns>
        [HttpGet("me")]
        public async Task<IActionResult> me()
        {
            return Ok();
        }

        /// <summary>
        /// edits my athlete basic info
        /// </summary>
        /// <param name="editDto">new values</param>
        /// <returns>OK, 400 for errors</returns>
        [HttpPost("me")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult editMe([FromBody] UserEditDto editDto)
        {
            return Ok();
        }

        /// <summary>
        /// gets the basic info of an athlete identified by the id param
        /// return 404 if no athlete with the given id is visible to the current user 
        /// (so the id might be valid but the currently logged in user does not have access rights)
        /// </summary>
        /// <param name="id">unique id of the athlete</param>
        /// <returns>OK</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(User), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> get(Guid id)
        {
            User t = await _sportDb.users.Include(x => x.homeAddressPlaceLink).Include(x => x.birthPlaceLink).FirstOrDefaultAsync(x => !x.isDeleted && x.athleteSet.Any(y => y.id == id && !y.isDeleted));
            return Ok(t);
        }

        /// <summary>
        /// edits an athlete's basic info
        /// </summary>
        /// <param name="editDto">new values</param>
        /// <returns>OK, 400 for errors, 404 for not found</returns>
        [HttpPost("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult edit(Guid id, [FromBody] UserEditDto editDto)
        {
            return Ok();
        }

        /// <summary>
        /// leave my club
        /// </summary>
        /// <returns>OK</returns>
        [HttpDelete("me")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult deleteMe()
        {
            return Ok();
        }

        /// <summary>
        /// send off an athlete from its current club (the athlete will become freelance)
        /// returns 404 if the currently logged in user can not fire an athlete with that id 
        /// (either because the athlete does not exists or its restricted)
        /// </summary>
        /// <param name="id">the athletes id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult delete(Guid id)
        {
            return Ok();
        }

        /// <summary>
        /// creates an user and make it an athlete
        /// </summary>
        /// <param name="createDto">user creation dto</param>
        /// <returns>userid of the new athlete</returns>
        [HttpPut]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(409)]
        public IActionResult create([FromBody] UserCreateDto createDto)
        {
            return Ok();
        }

    }
}
