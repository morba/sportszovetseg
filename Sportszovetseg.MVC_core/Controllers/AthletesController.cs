﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportszovetseg.DataAccess;
using Sportszovetseg.Models.Database;

namespace Sportszovetseg.MVC_core.Controllers
{
    public class AthletesController : Controller
    {
        private readonly SportDbContext _context;

        public AthletesController(SportDbContext context)
        {
            _context = context;
        }

        // GET: Athletes
        public async Task<IActionResult> Index()
        {
            var sportDbContext = _context.athletes.Include(a => a.userLink);
            return View(await sportDbContext.ToListAsync());
        }

        // GET: Athletes/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var athlete = await _context.athletes
                .Include(a => a.userLink)
                .SingleOrDefaultAsync(m => m.id == id);
            if (athlete == null)
            {
                return NotFound();
            }

            return View(athlete);
        }

        // GET: Athletes/Create
        public IActionResult Create()
        {
            ViewData["userId"] = new SelectList(_context.users, "id", "forename");
            return View();
        }

        // POST: Athletes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("userId,id,isDeleted")] Athlete athlete)
        {
            if (ModelState.IsValid)
            {
                athlete.id = Guid.NewGuid();
                _context.Add(athlete);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["userId"] = new SelectList(_context.users, "id", "forename", athlete.userId);
            return View(athlete);
        }

        // GET: Athletes/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var athlete = await _context.athletes.SingleOrDefaultAsync(m => m.id == id);
            if (athlete == null)
            {
                return NotFound();
            }
            ViewData["userId"] = new SelectList(_context.users, "id", "forename", athlete.userId);
            return View(athlete);
        }

        // POST: Athletes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("userId,id,isDeleted")] Athlete athlete)
        {
            if (id != athlete.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(athlete);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AthleteExists(athlete.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["userId"] = new SelectList(_context.users, "id", "forename", athlete.userId);
            return View(athlete);
        }

        // GET: Athletes/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var athlete = await _context.athletes
                .Include(a => a.userLink)
                .SingleOrDefaultAsync(m => m.id == id);
            if (athlete == null)
            {
                return NotFound();
            }

            return View(athlete);
        }

        // POST: Athletes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var athlete = await _context.athletes.SingleOrDefaultAsync(m => m.id == id);
            _context.athletes.Remove(athlete);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AthleteExists(Guid id)
        {
            return _context.athletes.Any(e => e.id == id);
        }
    }
}
