﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportszovetseg.Common;
using Sportszovetseg.DataAccess;
using Sportszovetseg.Models.Database;

namespace Sportszovetseg.MVC_core.Controllers
{
    public class UsersController : Controller
    {
        private readonly SportDbContext _context;

        public UsersController(SportDbContext context)
        {
            _context = context;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            var sportDbContext = _context.users.Include(u => u.birthPlaceLink).Include(u => u.homeAddressPlaceLink);
            return View(await sportDbContext.ToListAsync());
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.users
                .Include(u => u.birthPlaceLink)
                .Include(u => u.homeAddressPlaceLink)
                .SingleOrDefaultAsync(m => m.id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            ViewData["birthPlaceId"] = new SelectList(_context.places, "id", "address");
            ViewData["homeAddressId"] = new SelectList(_context.places, "id", "address");
            var t = Enum.GetValues(typeof(Language)).Cast<int>().Select(x => new EnumItem<int>() { value = x, name = ((Language)x).ToString() }).ToArray();
            ViewData["languages"] = new SelectList(Enum.GetValues(typeof(Language)).Cast<int>().Select(x=> new EnumItem<int>() { value=x, name = ((Language)x).ToString()}), "value", "name" );
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("userName,email,phoneNumber,birthdate,birthPlaceId,registrationDate,lastLoginDate,surname,forename,nationalityCode,homeAddressId,selectedLanguage,gender,id,isDeleted")] User user)
        {
            if (ModelState.IsValid)
            {
                user.id = Guid.NewGuid();
                _context.Add(user);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["birthPlaceId"] = new SelectList(_context.places, "id", "address", user.birthPlaceId);
            ViewData["homeAddressId"] = new SelectList(_context.places, "id", "address", user.homeAddressId);
            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.users.SingleOrDefaultAsync(m => m.id == id);
            if (user == null)
            {
                return NotFound();
            }
            ViewData["birthPlaceId"] = new SelectList(_context.places, "id", "address", user.birthPlaceId);
            ViewData["homeAddressId"] = new SelectList(_context.places, "id", "address", user.homeAddressId);
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("userName,email,phoneNumber,birthdate,birthPlaceId,registrationDate,lastLoginDate,surname,forename,nationalityCode,homeAddressId,selectedLanguage,gender,id,isDeleted")] User user)
        {
            if (id != user.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["birthPlaceId"] = new SelectList(_context.places, "id", "address", user.birthPlaceId);
            ViewData["homeAddressId"] = new SelectList(_context.places, "id", "address", user.homeAddressId);
            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.users
                .Include(u => u.birthPlaceLink)
                .Include(u => u.homeAddressPlaceLink)
                .SingleOrDefaultAsync(m => m.id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var user = await _context.users.SingleOrDefaultAsync(m => m.id == id);
            _context.users.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(Guid id)
        {
            return _context.users.Any(e => e.id == id);
        }
    }
}
