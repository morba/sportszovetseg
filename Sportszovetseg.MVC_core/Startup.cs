﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Sportszovetseg.BusinessLogic;
using Sportszovetseg.DataAccess;
using Sportszovetseg.Npgsql;

namespace Sportszovetseg.MVC_core
{
    public class Startup
    {
        private const string XForwardedPath = "X-Forwarded-Path";
        private const string XForwardedPathBase = "X-Forwarded-PathBase";
        private const string XForwardedProto = "X-Forwarded-Proto";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddLogging();
            services.AddOptions();
            IDbContextFactory<SportDbContext> dbContextFactory = new SportDbContextFactory(Configuration.GetConnectionString("main"));
            services.AddSingleton<IDbContextFactory<SportDbContext>>(dbContextFactory);
            services.AddDbContext<SportDbContext>(o => dbContextFactory.configureOptionsBuider(o));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app, 
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            SportDbContext sportDb)
        {
            //add data manually from proxy headers
            app.Use((context, next) =>
            {
                if (context.Request.Headers.TryGetValue(XForwardedPath, out StringValues path))
                {
                    context.Request.Path = new PathString(path);
                    //Console.WriteLine($"path={path}");
                }

                if (context.Request.Headers.TryGetValue(XForwardedPathBase, out StringValues pathBase))
                {
                    context.Request.PathBase = new PathString(pathBase);
                    //Console.WriteLine($"PathBase={pathBase}");
                }

                if (context.Request.Headers.TryGetValue(XForwardedProto, out StringValues proto))
                {
                    context.Request.Protocol = proto;
                    //Console.WriteLine($"proto={proto}");
                }
                return next();
            });

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders =
                  Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor
                | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto
                | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedHost
            });

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                loggerFactory.AddDebug();
                loggerFactory.AddEventSourceLogger();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            sportDb.Database.Migrate();
            sportDb.EnsureSeedData();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
