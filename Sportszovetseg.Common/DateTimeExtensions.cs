﻿using System;

namespace Sportszovetseg.Common
{
    public class EnumItem<T>
    {
        public T value;
        public string name;
    }

    public static class DateTimeExtensions
    {
        public static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static double ToEpochSeconds(this DateTime dtm) 
            => dtm.Kind == DateTimeKind.Local
            ? (dtm - epoch.ToLocalTime()).TotalSeconds
            : (dtm - epoch).TotalSeconds
            ;

    }
}
