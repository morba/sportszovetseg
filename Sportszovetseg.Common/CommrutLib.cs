﻿namespace Sportszovetseg.Common
{
    public static class CommrutLib
    {
        /// <summary>
        /// <para>byte array eqality function</para>
        /// <para>
        /// run time is independent of the data
        /// </para>
        /// <para>
        /// from: https://github.com/defuse/password-hashing/blob/master/PasswordStorage.cs
        /// </para>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool safe_equals(byte[] a, byte[] b)
        {
            uint diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
            {
                diff |= (uint)(a[i] ^ b[i]);
            }
            return diff == 0;
        }
    }
}
