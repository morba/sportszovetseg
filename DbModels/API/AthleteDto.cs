﻿using Sportszovetseg.Models.Database;
using System.ComponentModel.DataAnnotations;

namespace Sportszovetseg.Models.API
{

    /// <summary>
    /// DTO for editing an athlete's basic info
    /// </summary>
    public class UserEditDto
    {
        [MinLength(5)]
        [MaxLength(100)]
        [EmailAddress]
        public string email { get; set; }

        [MinLength(6)]
        [MaxLength(16)]
        [Phone]
        public string phoneNumber { get; set; }

        [MinLength(1)]
        [MaxLength(100)]
        public string surname { get; set; }

        [MinLength(1)]
        [MaxLength(100)]
        public string forename { get; set; }

        [MaxLength(3)]
        public string nationalityCode { get; set; }

        public Place homeAddress { get; set; }
    }
}
