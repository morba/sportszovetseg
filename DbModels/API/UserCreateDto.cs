﻿using Sportszovetseg.Models.Database;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sportszovetseg.Models.API
{
    /// <summary>
    /// DTO for creating an athlete (or an user)
    /// must be a new class for the required attributes
    /// </summary>
    public class UserCreateDto
    {
        [Required]
        [MinLength(5)]
        [MaxLength(100)]
        [EmailAddress]
        public string email { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(16)]
        [Phone]
        public string phoneNumber { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        public string surname { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        public string forename { get; set; }

        [Required]
        [MaxLength(3)]
        public string nationalityCode { get; set; }

        [Required]
        public Place homeAddress { get; set; }

        [Required]
        public DateTime birthdate { get; set; }

        public Place birthPlace { get; set; }

        public bool gender { get; set; }

        [Required]
        public string password { get; set; }
    }
}
