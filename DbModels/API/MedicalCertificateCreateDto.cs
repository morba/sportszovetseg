﻿using System;

namespace Sportszovetseg.Models.API
{
    /// <summary>
    /// DTO for creating a new medical certificate for an athlete
    /// </summary>
    public class MedicalCertificateCreateDto
    {
        /// <summary>
        /// document id of the preuploaded photo of the certificate
        /// </summary>
        public Guid documentId { get; set; }

        public DateTime validityStart { get; set; }

        public DateTime validityEnd { get; set; }
    }
}
