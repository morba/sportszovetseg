﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sportszovetseg.Models.Database
{
    /// <summary>
    /// location is reserved for gps
    /// but please come up with any better names you got
    /// </summary>
    public class Place : BaseEntity
    {
        [MaxLength(3)]
        [Required]
        public string countryCode { get; set; } = "HU";

        [Required]
        [MaxLength(100)]
        public string city { get; set; }

        [Required]
        [MaxLength(8)]
        public string zipCode { get; set; }

        /// <summary>
        /// everything else dumped here
        /// </summary>
        [Required]
        [MaxLength(1000)]
        public string address { get; set; }

        [JsonIgnore]
        public virtual ISet<User> user_birthplaceSet { get; set; } = new HashSet<User>();

        [JsonIgnore]
        public virtual ISet<User> user_homeAddressSet { get; set; } = new HashSet<User>();
    }


}
