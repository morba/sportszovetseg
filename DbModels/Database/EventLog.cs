﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sportszovetseg.Models.Database
{
    public class EventLog : BaseEntity
    {
        public enum Level
        {
            Debug = 0,
            Info = 1,
            Warning = 2,
            Error = 3,
            Critical = 4
        }

        [Required]
        [MaxLength(100)]
        public string eventName { get; set; }

        public DateTime timestamp { get; set; }

        public Level level { get; set; }

        [Required]
        public string message { get; set; }

        public string serializedException { get; set; }
    }


}
