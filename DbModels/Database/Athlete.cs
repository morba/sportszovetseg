﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    /// <summary>
    /// sportsman, i decided to create it even if its emnpty now.
    /// </summary>
    public class Athlete : BaseEntity
    {
        [ForeignKey(nameof(userLink))]
        public Guid userId { get; set; }

        public virtual User userLink { get; set; }

        public virtual ISet<AthleteClubMap> clubSet { get; set; } = new HashSet<AthleteClubMap>();
    }


}
