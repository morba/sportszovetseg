﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sportszovetseg.Models.Database
{
    public class Club : BaseEntity
    {
        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string name { get; set; }

        public virtual ISet<AthleteClubMap> athleteSet { get; set; }
    }


}
