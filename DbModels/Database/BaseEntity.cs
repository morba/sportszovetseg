﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sportszovetseg.Models.Database
{
    /// <summary>
    /// shared features of all db entites
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// everyone will have a primary key as a guid
        /// </summary>
        [Key]
        public Guid id { get; set; }

        /// <summary>
        /// everyone can be logically deleted
        /// </summary>
        public bool isDeleted { get; set; } = false;
    }
}
