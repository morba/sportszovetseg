﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    public class Competition : BaseEntity
    {
        [Required]
        [MinLength(6)]
        [MaxLength(100)]
        public string name { get; set; }

        public DateTime startDate { get; set; }

        public DateTime endDate { get; set; }

        [ForeignKey(nameof(placeLink))]
        public Guid placeId { get; set; }

        public Place placeLink { get; set; }

        public DateTime entryStart { get; set; }

        public DateTime entryEnd { get; set; }

        public decimal entryFee { get; set; }

        /// <summary>
        /// <para>teamSize = 1 individual athletes</para>
        /// <para>for simplicity team sizes are fixed and not a range as in real life</para>
        /// </summary>
        public int teamSize { get; set; } 
    }


}
