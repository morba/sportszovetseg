﻿using System;

namespace Sportszovetseg.Models.Database
{
    /// <summary>
    /// simple many to many connection
    /// </summary>
    public class AthleteEntryMap : BaseEntity
    {
        public Guid athleteId { get; set; }

        public virtual Athlete athleteLink { get; set; }

        public Guid entryId { get; set; }

        public virtual Entry entryLink { get; set; }
    }


}
