﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    /// <summary>
    /// very basic pw store
    /// missing: algorithm type, iteration count, forced password change timers, multifactor auth.
    /// </summary>
    public class Password : BaseEntity
    { 
        [ForeignKey(nameof(userLink))]
        public Guid userId { get; set; }

        public User userLink { get; set; }

        [Required]
        [MaxLength(32)]
        [MinLength(32)]
        public byte[] hash { get; set; }

        [Required]
        [MaxLength(32)]
        [MinLength(32)]
        public byte[] salt { get; set; }

        public DateTime lastAttempt { get; set; }

        public int attemptCounter { get; set; }
    }


}
