﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    public class User : BaseEntity
    {
        [MaxLength(12)]
        [MinLength(12)]
        [Required]
        public string userName { get; set; }

        [EmailAddress]
        [MaxLength(100)]
        public string email { get; set; }

        /// <summary>
        /// this could be better but i am too lazy
        /// </summary>
        [MaxLength(16)]
        [Phone]
        public string phoneNumber { get; set; }

        public DateTime birthdate { get; set; }

        /// <summary>
        /// optional
        /// </summary>
        [ForeignKey(nameof(birthPlaceLink))]
        public Guid? birthPlaceId { get; set; }

        public virtual Place birthPlaceLink { get; set; }

        public DateTime registrationDate { get; set; }

        public DateTime lastLoginDate { get; set; }

        /// <summary>
        /// family name
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string surname { get; set; }

        /// <summary>
        /// given name
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string forename { get; set; }

        /// <summary>
        /// this should really be an enum
        /// </summary>
        [Required]
        [MaxLength(3)]
        public string nationalityCode { get; set; }

        [ForeignKey(nameof(homeAddressPlaceLink))]
        public Guid homeAddressId { get; set; }

        public Language selectedLanguage { get; set; }

        /// <summary>
        /// <para>true for male, false for female</para>
        /// <para>has to be bool for maximum trigger capabilites</para>
        /// </summary>
        public bool gender { get; set; }

        public virtual Place homeAddressPlaceLink { get; set; }

        [JsonIgnore]
        public virtual ISet<Password> passwordSet { get; set; } = new HashSet<Password>();

        [JsonIgnore]
        public virtual ISet<Athlete> athleteSet { get; set; } = new HashSet<Athlete>();

        [JsonIgnore]
        public virtual ISet<RoleUserMap> roleSet { get; set; } = new HashSet<RoleUserMap>();

    }


}
