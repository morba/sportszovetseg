﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    public class MembershipFee : BaseEntity
    {
        [ForeignKey(nameof(clubLink))]
        public Guid clubId { get; set; }

        public Club clubLink { get; set; }

        [ForeignKey(nameof(documentLink))]
        public Guid documentId { get; set; }

        /// <summary>
        /// proof
        /// </summary>
        public Document documentLink { get; set; }

        /// <summary>
        /// lets ignore the currency for now
        /// </summary>
        public decimal amount { get; set; }

        public bool verified { get; set; }

        public DateTime start { get; set; }

        public DateTime end { get; set; }
    }


}
