﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sportszovetseg.Models.Database
{
    public class Role : BaseEntity
    {
        /// <summary>
        /// short name, for UI visualisation, NOT a description.
        /// </summary>
        [MaxLength(24)]
        public string name { get; set; }

        public virtual ISet<RoleUserMap> userSet { get; set; } = new HashSet<RoleUserMap>();
    }


}
