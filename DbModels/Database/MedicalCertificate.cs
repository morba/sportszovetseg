﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    public class MedicalCertificate : BaseEntity
    {
        [ForeignKey(nameof(athleteLink))]
        public Guid athleteId { get; set; }

        public Athlete athleteLink { get; set; } 

        [ForeignKey(nameof(documentLink))]
        public Guid documentId { get; set; }

        public Document documentLink { get; set; }

        /// <summary>
        /// all things must start somewhere (:
        /// </summary>
        public DateTime validityStart { get; set; }

        /// <summary>
        /// they all have a due date
        /// </summary>
        public DateTime validityEnd { get; set; }

        /// <summary>
        /// is it verified by the association?
        /// </summary>
        public bool verified { get; set; }
    }


}
