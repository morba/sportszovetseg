﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    public class Entry : BaseEntity
    {
        [ForeignKey(nameof(competitionLink))]
        public Guid competitionId { get; set; }

        public virtual Competition competitionLink { get; set; }

        [ForeignKey(nameof(clubLink))]
        public Guid clubId { get; set; }

        public virtual Club clubLink { get; set; }

        [Required]
        [MaxLength(100)]
        public string teamName { get; set; }

        public bool isEntryFeePaid { get; set; }

        [ForeignKey(nameof(entryFeeDocumentLink))]
        public Guid? entryFeeDocumentId { get; set; }

        public Document entryFeeDocumentLink { get; set; }
    }
}