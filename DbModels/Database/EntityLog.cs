﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    /// <summary>
    /// needs to be manually written
    /// </summary>
    public class EntityLog
    {
        [Key]
        public Guid id { get; set; }

        /// <summary>
        /// this probably cant be a foreign key...
        /// </summary>
        //[ForeignKey(nameof(entityLink))]
        public Guid entityId { get; set; }

        //public BaseEntity entityLink { get; set; }

        [ForeignKey(nameof(modifierUserLink))]
        public Guid modifierUser { get; set; }

        public User modifierUserLink { get; set; }

        public DateTime lastModified { get; set; }

        /// <summary>
        /// JSON state after the modification (the previous state can be found in the previous record)
        /// </summary>
        [Required]
        public string serializedState { get; set; }
    }


}
