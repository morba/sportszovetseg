﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sportszovetseg.Models.Database
{
    public class Parameters : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string key { get; set; }

        [Required]
        [MaxLength(1000)]
        public string value { get; set; }

        public DateTime start { get; set; }

        public DateTime emd { get; set; }
    }


}
