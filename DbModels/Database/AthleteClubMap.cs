﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    /// <summary>
    /// rename to contract ?
    /// </summary>
    public class AthleteClubMap : BaseEntity
    {

        [ForeignKey(nameof(athleteLink))]
        public Guid athleteId { get; set; }

        public Athlete athleteLink { get; set; }

        [ForeignKey(nameof(clubLink))]
        public Guid clubId { get; set; }

        public Club clubLink { get; set; }

        /// <summary>
        /// contract start date
        /// </summary>
        public DateTime start { get; set; }

        /// <summary>
        /// contract end date
        /// </summary>
        public DateTime end { get; set; }
    }


}
