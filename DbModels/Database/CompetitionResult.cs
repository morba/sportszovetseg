﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    public class CompetitionResult : BaseEntity
    {
        [ForeignKey(nameof(entryLink))]
        public Guid entryId { get; set; }

        public Entry entryLink { get; set; }

        /// <summary>
        /// just use -1 as DNF bcs i am a lazy f
        /// </summary>
        public int placement { get; set; }
    }


}
