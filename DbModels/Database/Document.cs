﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    public class Document : BaseEntity
    {
        public enum Type
        {
            /// <summary>
            /// = not set, useful for bug hunting, trust me!
            /// </summary>
            Null = 0,
            /// <summary>
            /// sportorvosi igazolás
            /// </summary>
            MedicalCertificate = 1,

            /// <summary>
            /// tagdíj, nevezési díj befizetéséről feltöltött igazolás
            /// </summary>
            PaymentCertificate = 2,
        }

        public Type type { get; set; }

        [Required]
        [MaxLength(100)]
        public string mimeType { get; set; }

        public long size { get; set; }

        /// <summary>
        /// only applicable to images
        /// </summary>
        public int width { get; set; }

        /// <summary>
        /// only applicable to images
        /// </summary>
        public int height { get; set; }

        /// <summary>
        /// sha256 hash
        /// </summary>
        [MinLength(32)]
        [MaxLength(32)]
        [Required]
        public byte[] sha256Hash { get; set; }

        /// <summary>
        /// use Select with anonim type or something to avoid returning this column if not needed
        /// </summary>
        [Required]
        public byte[] data { get; set; }

        [ForeignKey(nameof(uploaderUserLink))]
        public Guid uploaderUserId { get; set; }

        public virtual User uploaderUserLink { get; set; }

        [Timestamp]
        public DateTime timestamp { get; set; }
    }


}
