﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    public class RoleUserMap : BaseEntity
    {

        [ForeignKey(nameof(roleLink))]
        public Guid roleId { get; set; }

        public Role roleLink { get; set; }

        [ForeignKey(nameof(userLink))]
        public Guid userId { get; set; }

        public User userLink { get; set; }

        /// <summary>
        /// date of entry into force WTF english...
        /// </summary>
        public DateTime start { get; set; }

        public DateTime end { get; set; }
    }
}
