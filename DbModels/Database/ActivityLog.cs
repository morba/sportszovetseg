﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sportszovetseg.Models.Database
{
    public class ActivityLog : BaseEntity
    {
        public enum Action {
            /// <summary>
            /// should not happen
            /// </summary>
            Unspecified = 0,
            Create = 1,
            Modify = 2,
            Delete = 3,
        }

        [ForeignKey(nameof(userLink))]
        public Guid userId { get; set; }

        public virtual User userLink { get; set; }

        public DateTime timestamp { get; set; }

        public Guid changedEntityId { get; set; }

        [Required]
        [MaxLength(100)]
        public string changedEntityTypeName { get; set; }

        public Action action { get; set; }

        public string previusState { get; set; }

        [Required]
        public string nextState { get; set; }
    }


}
