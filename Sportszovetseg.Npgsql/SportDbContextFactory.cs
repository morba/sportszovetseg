﻿using Sportszovetseg.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;


namespace Sportszovetseg.Npgsql
{
    public class SportDbContextFactory : IDesignTimeDbContextFactory<SportDbContext>, IDbContextFactory<SportDbContext>
    {
        private string _connectionString;

        /// <summary>
        /// for command line migrations
        /// </summary>
        public SportDbContextFactory()
        {

        }

        public SportDbContextFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public SportDbContext CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<SportDbContext> builder = new DbContextOptionsBuilder<SportDbContext>();
            builder.UseNpgsql(_connectionString ?? Config.ConnectionString, b => b.MigrationsAssembly("Sportszovetseg.Npgsql"));
            return new SportDbContext(builder.Options);
        }

        public DbContextOptionsBuilder<SportDbContext> configureOptionsBuider(DbContextOptionsBuilder<SportDbContext> builder, string connectionString = null) 
            => builder.UseNpgsql(connectionString ?? _connectionString ?? Config.ConnectionString);

        public DbContextOptionsBuilder configureOptionsBuider(DbContextOptionsBuilder builder, string connectionString = null)
            => builder.UseNpgsql(connectionString ?? _connectionString ?? Config.ConnectionString, 
                b=>b.MigrationsAssembly("Sportszovetseg.DataAccess"));

        public SportDbContext createDbContext(string connectionString = null, DbContextOptionsBuilder<SportDbContext> builder = null)
            => new SportDbContext(
                (builder ?? new DbContextOptionsBuilder<SportDbContext>())
                .UseNpgsql(connectionString ?? _connectionString ?? Config.ConnectionString, 
                    b => b.MigrationsAssembly("Sportszovetseg.DataAccess")).Options);



    }
}
