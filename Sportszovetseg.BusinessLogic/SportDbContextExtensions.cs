﻿using System;
using System.Linq;
using Sportszovetseg.DataAccess;
using Sportszovetseg.Models.Database;

namespace Sportszovetseg.BusinessLogic
{
    public static class SportDbContextExtensions
    {
        /// <summary>
        /// ensures that the database contins the initial seed data
        /// </summary>
        /// <param name="sportDb"></param>
        public static void EnsureSeedData(this SportDbContext sportDb)
        {
            if(sportDb.places.Find(Guid.ParseExact("c08aa0fb-2988-48c7-8218-c913e37aae19", "D")) == null)
            {
                sportDb.places.Add(new Place
                {
                    id = Guid.ParseExact("c08aa0fb-2988-48c7-8218-c913e37aae19", "D"),
                    address = "Big Bang squere, 1",
                    city = "Universe",
                    countryCode = "UN",
                    isDeleted = false,
                    zipCode = "0000"
                });
            }

            if(sportDb.users.Find(Guid.ParseExact("f4833458-007a-4d1d-aaa2-c8eea51db17e", "D")) == null)
            {
                sportDb.users.Add(new User {
                    id = Guid.ParseExact("f4833458-007a-4d1d-aaa2-c8eea51db17e", "D"),
                    birthdate = DateTime.MinValue,
                    birthPlaceId = null,
                    email = "admin@do-not-send.asd",
                    forename = "Admin",
                    surname = "Administrator",
                    gender = false,
                    homeAddressId = Guid.ParseExact("c08aa0fb-2988-48c7-8218-c913e37aae19", "D"),
                    isDeleted = false,
                    lastLoginDate = DateTime.MinValue,
                    nationalityCode = "UN",
                    phoneNumber = "do-not-call-me",
                    registrationDate = DateTime.MinValue,
                    selectedLanguage = Language.en,
                    userName = "_i_am_groot_"
                });
            }

            if(sportDb.passwords.Find(Guid.ParseExact("ff8aa460-8299-4ca0-b774-20784eb69f86", "D")) == null)
            {
                Password pw1 = new Password {
                    id = Guid.ParseExact("ff8aa460-8299-4ca0-b774-20784eb69f86", "D"),
                    userId = Guid.ParseExact("f4833458-007a-4d1d-aaa2-c8eea51db17e", "D"),
                };
                PasswordManager.set_password("I don't know", ref pw1);
                sportDb.passwords.Add(pw1);
            }

            if(sportDb.roles.Find(Guid.ParseExact("e449a462-60a5-402e-8f6a-ab959b71e1cd", "D")) == null)
            {
                sportDb.roles.Add(new Role
                {
                    id = Guid.ParseExact("e449a462-60a5-402e-8f6a-ab959b71e1cd", "D"),
                    name = "root",
                    isDeleted = false
                });
            }
            if (sportDb.roles.Find(Guid.ParseExact("be5bb5c5-b3ff-4eed-8552-9da2f70373c9", "D")) == null)
            {
                sportDb.roles.Add(new Role
                {
                    id = Guid.ParseExact("be5bb5c5-b3ff-4eed-8552-9da2f70373c9", "D"),
                    name = "admin",
                    isDeleted = false
                });
            }
            if (sportDb.roles.Find(Guid.ParseExact("cfdca37f-7650-4a8b-87c1-55dc046aadb5", "D")) == null)
            {
                sportDb.roles.Add(new Role
                {
                    id = Guid.ParseExact("cfdca37f-7650-4a8b-87c1-55dc046aadb5", "D"),
                    name = "club",
                    isDeleted = false
                });
            }
            if (sportDb.roles.Find(Guid.ParseExact("aceddacf-2064-4761-ac1c-199df189cd70", "D")) == null)
            {
                sportDb.roles.Add(new Role
                {
                    id = Guid.ParseExact("aceddacf-2064-4761-ac1c-199df189cd70", "D"),
                    name = "athlete",
                    isDeleted = false
                });
            }
            if(sportDb.roleUserMaps.Find(Guid.ParseExact("a604e1b2-ba63-4220-9564-7c9d15d96269", "D")) == null)
            {
                //root
                sportDb.roleUserMaps.Add(new RoleUserMap
                {
                    id = Guid.ParseExact("a604e1b2-ba63-4220-9564-7c9d15d96269", "D"),
                    isDeleted = false,
                    roleId = Guid.ParseExact("e449a462-60a5-402e-8f6a-ab959b71e1cd", "D"),
                    userId = Guid.ParseExact("f4833458-007a-4d1d-aaa2-c8eea51db17e", "D"),
                    start = DateTime.MinValue,
                    end = DateTime.MaxValue,
                });
            }
            if (sportDb.roleUserMaps.Find(Guid.ParseExact("0e809a17-b81a-4546-ae14-86e08f8f3474", "D")) == null)
            {
                //admin
                sportDb.roleUserMaps.Add(new RoleUserMap
                {
                    id = Guid.ParseExact("0e809a17-b81a-4546-ae14-86e08f8f3474", "D"),
                    isDeleted = false,
                    roleId = Guid.ParseExact("be5bb5c5-b3ff-4eed-8552-9da2f70373c9", "D"),
                    userId = Guid.ParseExact("f4833458-007a-4d1d-aaa2-c8eea51db17e", "D"),
                    start = DateTime.MinValue,
                    end = DateTime.MaxValue,
                });
            }

            sportDb.SaveChanges();

        }

    }
}
