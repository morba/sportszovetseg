﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Sportszovetseg.Common;
using Sportszovetseg.Models.Database;

namespace Sportszovetseg.BusinessLogic
{
    public static class PasswordManager
    {
        private const int PW_HASH_BYTE_LENGTH = 18;
        private const int PW_SALT_BYTE_LENGTH = 24;
        private const int DEFAULT_ITERATION_COUNT = 64000;

        /// <summary>
        /// if password timeout attempts are above a given threshold then 
        /// we lock the user for attempting login for this amount of time
        /// given in seconds
        /// </summary>
        private const int LOCK_USER_IN_SECONDS = 5;

        /// <summary>
        /// the user has this amount of "free" password attempts after the
        /// limit is reached we take defensive actions ( lock the account for some time )
        /// </summary>
        private const int WRONG_PASSWORD_ATTEMPT_LIMIT = 3;

        public enum LoginResult
        {
            //uninitilized variable
            InternalError = 0,
            //OK
            Success = 1,
            //Fail
            UserNotfound = -1,
            UserDeleted = -2,
            PasswordNotFound = -3,
            UserLocked = -4,
            WrongPassword = -5,
        }

        /// <summary>
        /// try to login and give verbose output. 
        /// include passward too please
        /// </summary>
        /// <param name="user">user from db and include passward too please</param>
        /// <param name="password">the password to login with</param>
        /// <returns></returns>
        public static LoginResult login(ref User user, string password)
        {
            if (user == null)
            {
                return LoginResult.UserNotfound;
            }
            if (user.isDeleted)
            {
                return LoginResult.UserDeleted;
            }
            Password pw = user.passwordSet.FirstOrDefault(p => !p.isDeleted);
            if(pw == null)
            {
                return LoginResult.PasswordNotFound;
            }
            if(pw.attemptCounter > WRONG_PASSWORD_ATTEMPT_LIMIT && (DateTime.UtcNow - pw.lastAttempt).TotalSeconds < LOCK_USER_IN_SECONDS)
            {
                return LoginResult.UserLocked;
            }
            if(!try_login(password, ref pw))
            {
                return LoginResult.WrongPassword;
            }
            return LoginResult.Success;
        }


        internal static bool try_login(string password, ref Password pw)
        {
            pw.lastAttempt = DateTime.UtcNow;
            byte[] test_hash = new byte[PW_HASH_BYTE_LENGTH];
            using (Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, pw.salt))
            {
                pbkdf2.IterationCount = DEFAULT_ITERATION_COUNT;
                test_hash = pbkdf2.GetBytes(PW_HASH_BYTE_LENGTH);
            }
            if (CommrutLib.safe_equals(test_hash, pw.hash))
            {
                pw.attemptCounter = 0;
                return true;
            }
            else
            {
                pw.attemptCounter++;
                return false;
            }
        }

        public static void set_password(string newPassword, ref Password pw)
        {
            //reset
            pw.attemptCounter = 0;
            pw.lastAttempt = DateTime.MinValue;
            pw.salt = new byte[PW_SALT_BYTE_LENGTH];
            pw.hash = new byte[PW_HASH_BYTE_LENGTH];
            pw.isDeleted = false;

            //create
            using (RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider())
            {
                csprng.GetBytes(pw.salt);
            }
            using (Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(newPassword, pw.salt))
            {
                pbkdf2.IterationCount = DEFAULT_ITERATION_COUNT;
                pw.hash = pbkdf2.GetBytes(PW_HASH_BYTE_LENGTH);
            }
        }

    }
}
