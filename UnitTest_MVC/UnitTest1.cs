﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sportszovetseg.DataAccess;
using Sportszovetseg.Models.Database;
using Sportszovetseg.MVC_core.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace UnitTest_MVC
{
    public static class TestData1
    {
        public static readonly Place[] places = new Place[]
        {
            new Place {
                id = Guid.ParseExact("73f1ae20-d88c-4c2e-b078-e7db36dad61b", "D"),
                address = "foo bar",
                city = "none",
                countryCode = "NO",
                zipCode = "1234",
                isDeleted = false,
            },
            new Place
            {
                id = Guid.ParseExact("0d45cec1-6a00-4522-8cf8-1226fef8fb66", "D"),
                address = "3705 Caynor Circle",
                city = "Newark",
                zipCode = "07102",
                countryCode = "US"
            },

        };
    }

    [TestClass]
    public class UnitTestPlacesController_ReadOnly
    {
        private DbContextOptionsBuilder<SportDbContext> dbBuilder;
        private SportDbContext sportDb;

        [TestInitialize]
        public void init()
        {
            if (sportDb == null)
            {
                dbBuilder = new DbContextOptionsBuilder<SportDbContext>().UseInMemoryDatabase("places_test_index");
                sportDb = new SportDbContext(dbBuilder.Options);
                if (!sportDb.places.Any())
                {
                    sportDb.places.AddRange(TestData1.places);
                    sportDb.SaveChanges();
                }
            }
        }

        [TestMethod]
        public async Task Index_ReturnsAViewResult_WithAListOfPlaces()
        {
            //arrange
            var controller = new PlacesController(sportDb);

            //act
            var result = await controller.Index();

            //assert
            Assert.AreEqual(typeof(ViewResult), result.GetType());
            var viewResult = result as ViewResult;
            Assert.IsTrue(typeof(IEnumerable<Place>).IsInstanceOfType(viewResult.ViewData.Model));
            var model = viewResult.ViewData.Model as IEnumerable<Place>;
            Assert.AreEqual(2, model.Count());
        }

        [TestMethod]
        public async Task Details_NotFound()
        {
            //arrange
            var controller = new PlacesController(sportDb);

            //act
            var result = await controller.Details(Guid.Empty);

            //assert
            Assert.AreEqual(typeof(NotFoundResult), result.GetType());
        }

        [TestMethod]
        public async Task Details_ExistingObject()
        {
            //arrange
            var controller = new PlacesController(sportDb);

            //act
            var result = await controller.Details(Guid.ParseExact("0d45cec1-6a00-4522-8cf8-1226fef8fb66", "D"));

            //assert
            Assert.AreEqual(typeof(ViewResult), result.GetType());
            var viewResult = result as ViewResult;
            Assert.IsTrue(typeof(Place).IsInstanceOfType(viewResult.ViewData.Model));
            var model = viewResult.ViewData.Model as Place;
            Assert.AreEqual("3705 Caynor Circle", model.address);
            Assert.AreEqual("Newark", model.city);
            Assert.AreEqual("US", model.countryCode);
            Assert.AreEqual("07102", model.zipCode);
        }

        [TestMethod]
        public async Task Edit_Get_NotFound()
        {
            //arrange
            var controller = new PlacesController(sportDb);

            //act
            var result = await controller.Edit(Guid.Empty);

            //assert
            Assert.AreEqual(typeof(NotFoundResult), result.GetType());
        }

        [TestMethod]
        public async Task Edit_ExistingObject()
        {
            //arrange
            var controller = new PlacesController(sportDb);

            //act
            var result = await controller.Edit(Guid.ParseExact("0d45cec1-6a00-4522-8cf8-1226fef8fb66", "D"));

            //assert
            Assert.AreEqual(typeof(ViewResult), result.GetType());
            var viewResult = result as ViewResult;
            Assert.IsTrue(typeof(Place).IsInstanceOfType(viewResult.ViewData.Model));
            var model = viewResult.ViewData.Model as Place;
            Assert.AreEqual("3705 Caynor Circle", model.address);
            Assert.AreEqual("Newark", model.city);
            Assert.AreEqual("US", model.countryCode);
            Assert.AreEqual("07102", model.zipCode);
        }

        [TestMethod]
        public async Task Edit_Post_NotFound()
        {
            //arrange
            var controller = new PlacesController(sportDb);

            //act
            var result = await controller.Edit(Guid.Empty, new Place {
                id = Guid.ParseExact("b1b08279-dc67-45f4-a20c-364ba50cf16a", "D"),
                address = "2926 Hartway Street",
                city = "Aberdeen",
                countryCode = "US",
                zipCode = "57401"
            });

            //assert
            Assert.AreEqual(typeof(NotFoundResult), result.GetType());
        }

        [TestMethod]
        public async Task Delete_ExistingObject()
        {
            //arrange
            var controller = new PlacesController(sportDb);

            //act
            var result = await controller.Delete(Guid.ParseExact("0d45cec1-6a00-4522-8cf8-1226fef8fb66", "D"));

            //assert
            Assert.AreEqual(typeof(ViewResult), result.GetType());
            var viewResult = result as ViewResult;
            Assert.IsTrue(typeof(Place).IsInstanceOfType(viewResult.ViewData.Model));
            var model = viewResult.ViewData.Model as Place;
            Assert.AreEqual("3705 Caynor Circle", model.address);
            Assert.AreEqual("Newark", model.city);
            Assert.AreEqual("US", model.countryCode);
            Assert.AreEqual("07102", model.zipCode);
        }

        [TestMethod]
        public async Task Delete_NotFound()
        {
            //arrange
            var controller = new PlacesController(sportDb);

            //act
            var result = await controller.Delete(Guid.Empty);

            //assert
            Assert.AreEqual(typeof(NotFoundResult), result.GetType());
        }

    }
    [TestClass]
    public class UnitTestPlacesController_Create
    {
        private DbContextOptionsBuilder<SportDbContext> dbBuilder;
        private SportDbContext sportDb;

        [TestInitialize]
        public void init()
        {
            dbBuilder = new DbContextOptionsBuilder<SportDbContext>().UseInMemoryDatabase("places_test_create");
            sportDb = new SportDbContext(dbBuilder.Options);
            sportDb.places.AddRange(TestData1.places);
            sportDb.SaveChanges();
        }

        [TestMethod]
        public async Task Create_SavesToDatabaseCorrectly()
        {
            //arrange
            var controller = new PlacesController(sportDb);

            //act
            var result = await controller.Create(new Place {
                address = "2926 Hartway Street",
                city = "Aberdeen",
                countryCode = "US",
                zipCode = "57401"
            });

            //assert
            
            Assert.AreEqual(typeof(RedirectToActionResult), result.GetType());
            var viewResult = result as RedirectToActionResult;
            Assert.AreEqual("Index", viewResult.ActionName, true);
            Assert.AreEqual(3, sportDb.places.Count());
            var the_saved_obj = sportDb.places.FirstOrDefault(p => p.address == "2926 Hartway Street");
            Assert.IsNotNull(the_saved_obj);
            Assert.AreEqual("Aberdeen", the_saved_obj.city);
            Assert.AreEqual("57401", the_saved_obj.zipCode);
            Assert.AreEqual("US", the_saved_obj.countryCode);
        }
    }

    [TestClass]
    public class UnitTestPlacesController_Edit
    {
        private DbContextOptionsBuilder<SportDbContext> dbBuilder;
        private SportDbContext sportDb;

        [TestInitialize]
        public void init()
        {
            dbBuilder = new DbContextOptionsBuilder<SportDbContext>().UseInMemoryDatabase("places_test_edit");
            sportDb = new SportDbContext(dbBuilder.Options);
            sportDb.places.AddRange(TestData1.places);
            sportDb.SaveChanges();
        }

        [TestMethod]
        public async Task Edit_SavesToDatabaseCorrectly()
        {
            //arrange
            var controller = new PlacesController(sportDb);
            var id = Guid.ParseExact("0d45cec1-6a00-4522-8cf8-1226fef8fb66", "D");

            //act 1
            var result0 = await controller.Edit(id);

            //assert 1
            Assert.AreEqual(typeof(ViewResult), result0.GetType());
            var viewResult0 = result0 as ViewResult;
            Assert.IsTrue(typeof(Place).IsInstanceOfType(viewResult0.ViewData.Model));
            var model = viewResult0.ViewData.Model as Place;

            //act 2
            model.address = "2926 Hartway Street";
            model.city = "Aberdeen";
            model.zipCode = "57401";
            var result = await controller.Edit(id, model);

            //assert 2
            Assert.AreEqual(typeof(RedirectToActionResult), result.GetType());
            var viewResult = result as RedirectToActionResult;
            Assert.AreEqual("Index", viewResult.ActionName, true);
            Assert.AreEqual(2, sportDb.places.Count());
            var the_saved_obj = sportDb.places.FirstOrDefault(p => p.id == id);
            Assert.IsNotNull(the_saved_obj);
            Assert.AreEqual("2926 Hartway Street", the_saved_obj.address);
            Assert.AreEqual("Aberdeen", the_saved_obj.city);
            Assert.AreEqual("57401", the_saved_obj.zipCode);
            Assert.AreEqual("US", the_saved_obj.countryCode);
        }
    }

    [TestClass]
    public class UnitTestPlacesController_Edit_2
    {
        private DbContextOptionsBuilder<SportDbContext> dbBuilder;
        private SportDbContext sportDb;

        [TestInitialize]
        public void init()
        {
            dbBuilder = new DbContextOptionsBuilder<SportDbContext>().UseInMemoryDatabase("places_test_edit2");
            sportDb = new SportDbContext(dbBuilder.Options);
            sportDb.places.AddRange(TestData1.places);
            sportDb.SaveChanges();
        }

        /// <summary>
        /// who ever started to edit the object later will overwrite sll changes made by others. (even if they hit the save button later)
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Edit_ConcurrentSavesToDatabase()
        {
            //arrange
            var controller = new PlacesController(sportDb);
            var id = Guid.ParseExact("0d45cec1-6a00-4522-8cf8-1226fef8fb66", "D");

            //act 1
            var result0 = await controller.Edit(id);
            var result1 = await controller.Edit(id);

            //assert 1
            Assert.AreEqual(typeof(ViewResult), result0.GetType());
            var viewResult0 = result0 as ViewResult;
            Assert.IsTrue(typeof(Place).IsInstanceOfType(viewResult0.ViewData.Model));
            var model = viewResult0.ViewData.Model as Place;

            Assert.AreEqual(typeof(ViewResult), result1.GetType());
            var viewResult1 = result1 as ViewResult;
            Assert.IsTrue(typeof(Place).IsInstanceOfType(viewResult1.ViewData.Model));
            var model1 = viewResult0.ViewData.Model as Place;

            //act 2
            model.address = "2926 Hartway Street";
            model.city = "Aberdeen";
            model.zipCode = "57401";

            model1.address = "Fő út 69.";
            model1.city = "Szecső";
            model1.zipCode = "5741";
            model1.countryCode = "HU";

            var task2 = controller.Edit(id, model1);
            Thread.Sleep(5000);
            var task1 = controller.Edit(id, model);


            //await Task.WaitAll(task1, task2);

            var result2 = await task2;
            var result = await task1;
            

            //assert 2
            Assert.AreEqual(typeof(RedirectToActionResult), result.GetType());
            var viewResult = result as RedirectToActionResult;
            Assert.AreEqual("Index", viewResult.ActionName, true);
            Assert.AreEqual(2, sportDb.places.Count());
            Assert.AreEqual(typeof(RedirectToActionResult), result2.GetType());
            var viewResult2 = result2 as RedirectToActionResult;
            Assert.AreEqual("Index", viewResult2.ActionName, true);

            var the_saved_obj = sportDb.places.FirstOrDefault(p => p.id == id);
            Assert.IsNotNull(the_saved_obj);
            Assert.AreEqual("Fő út 69.", the_saved_obj.address);
            Assert.AreEqual("Szecső", the_saved_obj.city);
            Assert.AreEqual("5741", the_saved_obj.zipCode);
            Assert.AreEqual("HU", the_saved_obj.countryCode);
        }

    }

}
