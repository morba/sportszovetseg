﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using Microsoft.EntityFrameworkCore;
using Sportszovetseg.DataAccess;
using IdentityServer4.Services;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Extensions;
using System;
using System.Linq;
using System.Security.Claims;

namespace Sportszovetseg.IDP
{
    public class SportUserProfileService : IProfileService
    {
        private readonly SportDbContext _db;

        public SportUserProfileService(SportDbContext db)
        {
            _db = db;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var subjectId = context.Subject.GetSubjectId();
            var userId = Guid.ParseExact(subjectId, "D");
            var user = await _db.users.Include(u => u.roleSet).SingleOrDefaultAsync(x => x.id == userId);
            var roles = await _db.roles.Where(r => r.userSet.Any(u => u.userId == userId)).ToListAsync();
            //TODO
            context.IssuedClaims = roles.Select
              (r => new Claim("role", r.name)).ToList();

        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var subjectId = context.Subject.GetSubjectId();
            if (!Guid.TryParse(subjectId, out Guid id))
            {
                context.IsActive = false;
            }
            else
            {
                context.IsActive = !((await _db.users.FindAsync(id))?.isDeleted ?? true);
            }

        }
    }
}