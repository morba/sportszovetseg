﻿//system
//3rd party
using Microsoft.EntityFrameworkCore;
//project

namespace Sportszovetseg.DataAccess
{
    public interface IDbContextfactory
    {
        DbContextOptionsBuilder configureOptionsBuider(DbContextOptionsBuilder builder, string connectionString = null);
    }

    public interface IDbContextFactory<TContext> : IDbContextfactory where TContext: DbContext
    {
        DbContextOptionsBuilder<TContext>  configureOptionsBuider(DbContextOptionsBuilder<TContext> builder, string connectionString = null);

        TContext createDbContext(string connectionString = null, DbContextOptionsBuilder<TContext> builder = null);
    }
}
