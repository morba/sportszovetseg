﻿//system
using System;
//3rd party
using Microsoft.EntityFrameworkCore;
//project
using Sportszovetseg.Models.Database;

namespace Sportszovetseg.DataAccess
{
    public class SportDbContext : DbContext
    {
        /// <summary>
        /// default ctor. use with DI
        /// </summary>
        /// <param name="options"></param>
        public SportDbContext(DbContextOptions<SportDbContext> options) : base(options)
        {

        }

        public DbSet<ActivityLog> activityLogs { get; set; }

        public DbSet<Athlete> athletes { get; set; }

        public DbSet<AthleteClubMap> athleteClubMaps { get; set; }

        public DbSet<AthleteEntryMap> athleteEntryMaps { get; set; }

        public DbSet<Club> clubs { get; set; }

        public DbSet<Competition> competitions { get; set; }

        public DbSet<CompetitionResult> competitionResults { get; set; }

        public DbSet<Document> documents { get; set; }

        public DbSet<EntityLog> entityLogs { get; set; }

        public DbSet<Entry> entries { get; set; }

        public DbSet<EventLog> eventLogs { get; set; }

        public DbSet<MedicalCertificate> medicalCertificates { get; set; }

        public DbSet<MembershipFee> membershipFees { get; set; }

        public DbSet<Parameters> parameters { get; set; }

        public DbSet<Password> passwords { get; set; }

        public DbSet<Place> places { get; set; }

        public DbSet<Role> roles { get; set; }

        public DbSet<RoleUserMap> roleUserMaps { get; set; }

        public DbSet<User> users { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Place>()
                .HasMany(place => place.user_birthplaceSet)
                .WithOne(user => user.birthPlaceLink)
                .HasForeignKey(user => user.birthPlaceId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict)
                ;

            builder.Entity<Place>()
                .HasMany(place => place.user_homeAddressSet)
                .WithOne(user => user.homeAddressPlaceLink)
                .HasForeignKey(user => user.homeAddressId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Restrict)
                ;
        }

    }
}
