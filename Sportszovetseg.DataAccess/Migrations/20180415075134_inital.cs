﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sportszovetseg.DataAccess.Migrations
{
    public partial class inital : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "clubs",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_clubs", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "eventLogs",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    eventName = table.Column<string>(maxLength: 100, nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    level = table.Column<int>(nullable: false),
                    message = table.Column<string>(nullable: false),
                    serializedException = table.Column<string>(nullable: true),
                    timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_eventLogs", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "parameters",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    emd = table.Column<DateTime>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    key = table.Column<string>(maxLength: 100, nullable: false),
                    start = table.Column<DateTime>(nullable: false),
                    value = table.Column<string>(maxLength: 1000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_parameters", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "places",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    address = table.Column<string>(maxLength: 1000, nullable: false),
                    city = table.Column<string>(maxLength: 100, nullable: false),
                    countryCode = table.Column<string>(maxLength: 3, nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    zipCode = table.Column<string>(maxLength: 8, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_places", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "roles",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 24, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_roles", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "competitions",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    endDate = table.Column<DateTime>(nullable: false),
                    entryEnd = table.Column<DateTime>(nullable: false),
                    entryFee = table.Column<decimal>(nullable: false),
                    entryStart = table.Column<DateTime>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 100, nullable: false),
                    placeId = table.Column<Guid>(nullable: false),
                    startDate = table.Column<DateTime>(nullable: false),
                    teamSize = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_competitions", x => x.id);
                    table.ForeignKey(
                        name: "FK_competitions_places_placeId",
                        column: x => x.placeId,
                        principalTable: "places",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    birthPlaceId = table.Column<Guid>(nullable: true),
                    birthdate = table.Column<DateTime>(nullable: false),
                    email = table.Column<string>(maxLength: 100, nullable: true),
                    forename = table.Column<string>(maxLength: 100, nullable: false),
                    gender = table.Column<bool>(nullable: false),
                    homeAddressId = table.Column<Guid>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    lastLoginDate = table.Column<DateTime>(nullable: false),
                    nationalityCode = table.Column<string>(maxLength: 3, nullable: false),
                    phoneNumber = table.Column<string>(maxLength: 16, nullable: true),
                    registrationDate = table.Column<DateTime>(nullable: false),
                    selectedLanguage = table.Column<int>(nullable: false),
                    surname = table.Column<string>(maxLength: 100, nullable: false),
                    userName = table.Column<string>(maxLength: 12, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.id);
                    table.ForeignKey(
                        name: "FK_users_places_birthPlaceId",
                        column: x => x.birthPlaceId,
                        principalTable: "places",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_users_places_homeAddressId",
                        column: x => x.homeAddressId,
                        principalTable: "places",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "activityLogs",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    action = table.Column<int>(nullable: false),
                    changedEntityId = table.Column<Guid>(nullable: false),
                    changedEntityTypeName = table.Column<string>(maxLength: 100, nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    nextState = table.Column<string>(nullable: false),
                    previusState = table.Column<string>(nullable: true),
                    timestamp = table.Column<DateTime>(nullable: false),
                    userId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_activityLogs", x => x.id);
                    table.ForeignKey(
                        name: "FK_activityLogs_users_userId",
                        column: x => x.userId,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "athletes",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    userId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_athletes", x => x.id);
                    table.ForeignKey(
                        name: "FK_athletes_users_userId",
                        column: x => x.userId,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "documents",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    data = table.Column<byte[]>(nullable: false),
                    height = table.Column<int>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    mimeType = table.Column<string>(maxLength: 100, nullable: false),
                    sha256Hash = table.Column<byte[]>(maxLength: 32, nullable: false),
                    size = table.Column<long>(nullable: false),
                    timestamp = table.Column<DateTime>(nullable: false),
                    type = table.Column<int>(nullable: false),
                    uploaderUserId = table.Column<Guid>(nullable: false),
                    width = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_documents", x => x.id);
                    table.ForeignKey(
                        name: "FK_documents_users_uploaderUserId",
                        column: x => x.uploaderUserId,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "entityLogs",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    entityId = table.Column<Guid>(nullable: false),
                    lastModified = table.Column<DateTime>(nullable: false),
                    modifierUser = table.Column<Guid>(nullable: false),
                    serializedState = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_entityLogs", x => x.id);
                    table.ForeignKey(
                        name: "FK_entityLogs_users_modifierUser",
                        column: x => x.modifierUser,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "passwords",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    attemptCounter = table.Column<int>(nullable: false),
                    hash = table.Column<byte[]>(maxLength: 32, nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    lastAttempt = table.Column<DateTime>(nullable: false),
                    salt = table.Column<byte[]>(maxLength: 32, nullable: false),
                    userId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_passwords", x => x.id);
                    table.ForeignKey(
                        name: "FK_passwords_users_userId",
                        column: x => x.userId,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "roleUserMaps",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    end = table.Column<DateTime>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    roleId = table.Column<Guid>(nullable: false),
                    start = table.Column<DateTime>(nullable: false),
                    userId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_roleUserMaps", x => x.id);
                    table.ForeignKey(
                        name: "FK_roleUserMaps_roles_roleId",
                        column: x => x.roleId,
                        principalTable: "roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_roleUserMaps_users_userId",
                        column: x => x.userId,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "athleteClubMaps",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    athleteId = table.Column<Guid>(nullable: false),
                    clubId = table.Column<Guid>(nullable: false),
                    end = table.Column<DateTime>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    start = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_athleteClubMaps", x => x.id);
                    table.ForeignKey(
                        name: "FK_athleteClubMaps_athletes_athleteId",
                        column: x => x.athleteId,
                        principalTable: "athletes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_athleteClubMaps_clubs_clubId",
                        column: x => x.clubId,
                        principalTable: "clubs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "entries",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    clubId = table.Column<Guid>(nullable: false),
                    competitionId = table.Column<Guid>(nullable: false),
                    entryFeeDocumentId = table.Column<Guid>(nullable: true),
                    isDeleted = table.Column<bool>(nullable: false),
                    isEntryFeePaid = table.Column<bool>(nullable: false),
                    teamName = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_entries", x => x.id);
                    table.ForeignKey(
                        name: "FK_entries_clubs_clubId",
                        column: x => x.clubId,
                        principalTable: "clubs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_entries_competitions_competitionId",
                        column: x => x.competitionId,
                        principalTable: "competitions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_entries_documents_entryFeeDocumentId",
                        column: x => x.entryFeeDocumentId,
                        principalTable: "documents",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "medicalCertificates",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    athleteId = table.Column<Guid>(nullable: false),
                    documentId = table.Column<Guid>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    validityEnd = table.Column<DateTime>(nullable: false),
                    validityStart = table.Column<DateTime>(nullable: false),
                    verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_medicalCertificates", x => x.id);
                    table.ForeignKey(
                        name: "FK_medicalCertificates_athletes_athleteId",
                        column: x => x.athleteId,
                        principalTable: "athletes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_medicalCertificates_documents_documentId",
                        column: x => x.documentId,
                        principalTable: "documents",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "membershipFees",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    amount = table.Column<decimal>(nullable: false),
                    clubId = table.Column<Guid>(nullable: false),
                    documentId = table.Column<Guid>(nullable: false),
                    end = table.Column<DateTime>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    start = table.Column<DateTime>(nullable: false),
                    verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_membershipFees", x => x.id);
                    table.ForeignKey(
                        name: "FK_membershipFees_clubs_clubId",
                        column: x => x.clubId,
                        principalTable: "clubs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_membershipFees_documents_documentId",
                        column: x => x.documentId,
                        principalTable: "documents",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "athleteEntryMaps",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    athleteId = table.Column<Guid>(nullable: false),
                    entryId = table.Column<Guid>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_athleteEntryMaps", x => x.id);
                    table.ForeignKey(
                        name: "FK_athleteEntryMaps_athletes_athleteId",
                        column: x => x.athleteId,
                        principalTable: "athletes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_athleteEntryMaps_entries_entryId",
                        column: x => x.entryId,
                        principalTable: "entries",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "competitionResults",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    entryId = table.Column<Guid>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    placement = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_competitionResults", x => x.id);
                    table.ForeignKey(
                        name: "FK_competitionResults_entries_entryId",
                        column: x => x.entryId,
                        principalTable: "entries",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_activityLogs_userId",
                table: "activityLogs",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_athleteClubMaps_athleteId",
                table: "athleteClubMaps",
                column: "athleteId");

            migrationBuilder.CreateIndex(
                name: "IX_athleteClubMaps_clubId",
                table: "athleteClubMaps",
                column: "clubId");

            migrationBuilder.CreateIndex(
                name: "IX_athleteEntryMaps_athleteId",
                table: "athleteEntryMaps",
                column: "athleteId");

            migrationBuilder.CreateIndex(
                name: "IX_athleteEntryMaps_entryId",
                table: "athleteEntryMaps",
                column: "entryId");

            migrationBuilder.CreateIndex(
                name: "IX_athletes_userId",
                table: "athletes",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_competitionResults_entryId",
                table: "competitionResults",
                column: "entryId");

            migrationBuilder.CreateIndex(
                name: "IX_competitions_placeId",
                table: "competitions",
                column: "placeId");

            migrationBuilder.CreateIndex(
                name: "IX_documents_uploaderUserId",
                table: "documents",
                column: "uploaderUserId");

            migrationBuilder.CreateIndex(
                name: "IX_entityLogs_modifierUser",
                table: "entityLogs",
                column: "modifierUser");

            migrationBuilder.CreateIndex(
                name: "IX_entries_clubId",
                table: "entries",
                column: "clubId");

            migrationBuilder.CreateIndex(
                name: "IX_entries_competitionId",
                table: "entries",
                column: "competitionId");

            migrationBuilder.CreateIndex(
                name: "IX_entries_entryFeeDocumentId",
                table: "entries",
                column: "entryFeeDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_medicalCertificates_athleteId",
                table: "medicalCertificates",
                column: "athleteId");

            migrationBuilder.CreateIndex(
                name: "IX_medicalCertificates_documentId",
                table: "medicalCertificates",
                column: "documentId");

            migrationBuilder.CreateIndex(
                name: "IX_membershipFees_clubId",
                table: "membershipFees",
                column: "clubId");

            migrationBuilder.CreateIndex(
                name: "IX_membershipFees_documentId",
                table: "membershipFees",
                column: "documentId");

            migrationBuilder.CreateIndex(
                name: "IX_passwords_userId",
                table: "passwords",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_roleUserMaps_roleId",
                table: "roleUserMaps",
                column: "roleId");

            migrationBuilder.CreateIndex(
                name: "IX_roleUserMaps_userId",
                table: "roleUserMaps",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_users_birthPlaceId",
                table: "users",
                column: "birthPlaceId");

            migrationBuilder.CreateIndex(
                name: "IX_users_homeAddressId",
                table: "users",
                column: "homeAddressId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "activityLogs");

            migrationBuilder.DropTable(
                name: "athleteClubMaps");

            migrationBuilder.DropTable(
                name: "athleteEntryMaps");

            migrationBuilder.DropTable(
                name: "competitionResults");

            migrationBuilder.DropTable(
                name: "entityLogs");

            migrationBuilder.DropTable(
                name: "eventLogs");

            migrationBuilder.DropTable(
                name: "medicalCertificates");

            migrationBuilder.DropTable(
                name: "membershipFees");

            migrationBuilder.DropTable(
                name: "parameters");

            migrationBuilder.DropTable(
                name: "passwords");

            migrationBuilder.DropTable(
                name: "roleUserMaps");

            migrationBuilder.DropTable(
                name: "entries");

            migrationBuilder.DropTable(
                name: "athletes");

            migrationBuilder.DropTable(
                name: "roles");

            migrationBuilder.DropTable(
                name: "clubs");

            migrationBuilder.DropTable(
                name: "competitions");

            migrationBuilder.DropTable(
                name: "documents");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "places");
        }
    }
}
